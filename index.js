var data2 = {"GridData":
  [{"Type":"Folder", "Name":"Sub Folder", "Description":"Sub Folder Description", "ModifiedDate":"1/21/2014 2:56 PM",},
  {"Type":"Folder", "Name":"Test 2", "Description":"", "ModifiedDate":"1/21/2014 2:54 PM"},
  {"Type":"Folder", "Name":"Test", "Description":"", "ModifiedDate":"1/20/2014 1:17 PM"},
  {"Type":"Folder", "Name":"New Folder", "Description":"", "ModifiedDate":"1/21/2014 2:53 PM"},
  {"Type":"Document", "Name":"Send Documents Usability.docx", "Description":"Document Description", "ModifiedDate":"1/21/2014 3:55 PM"},
  {"Type":"Document", "Name":"Star Image.png", "Description":"", "ModifiedDate":"1/21/2014 3:45 PM"},
  {"Type":"Document", "Name":"Coming Soon Text.docx", "Description":"", "ModifiedDate":"1/21/2014 3:15 PM"},
  {"Type":"Document", "Name":"Internationalization_Notes.pdf", "Description":"", "ModifiedDate":"1/21/2014 2:58 PM"},
  {"Type":"Document", "Name":"Test Document.docx", "Description":"This is a test document", "ModifiedDate":"1/21/2014 2:55 PM"},
  {"Type":"Document", "Name":"Spreadsheet.xlsx", "Description":"", "ModifiedDate":"1/21/2014 2:35 PM"},
  {"Type":"Document", "Name":"Moon Image.png", "Description":"", "ModifiedDate":"1/21/2014 2:27 PM"},
  {"Type":"Document", "Name":"What is this.pdf", "Description":"", "ModifiedDate":"3/21/2015 9:54 AM"},
  {"Type":"Document", "Name":"Json Generator.exe", "Description":"", "ModifiedDate":"8/21/2015 7:23 PM"},
  {"Type":"Document", "Name":"Code Exercise.zip", "Description":"", "ModifiedDate":"7/22/2015 4:13 PM"},
  {"Type":"Document", "Name":"Marketting Banner.jpeg", "Description":"For Home Page", "ModifiedDate":"4/5/2014 2:19 PM"},
  {"Type":"Document", "Name":"Presentation Deck.ppt", "Description":"", "ModifiedDate":"1/21/2014 1:37 PM"}],
  "ActionButtons":
  [{"Name":"Upload", "ImageName":"upload"},
  {"Name":"Create Folder", "ImageName":"create_folder"},
  {"Name":"Share", "ImageName":"share"},
  {"Name":"Cut", "ImageName":"cut"},
  {"Name":"Manage Attributes", "ImageName":"tag"},
  {"Name":"Copy", "ImageName":"copy"},
  {"Name":"Paste", "ImageName":"paste"},
  {"Name":"Search", "ImageName":"search"},
  {"Name":"Folder Settings", "ImageName":"folder_settings"},
  {"Name":"Send Email", "ImageName":"send_email"}]
}

$(document).ready(function() {

  renderInterface()

  function renderInterface() {
    $.getJSON('/data.json', function(data, textStatus) {
        renderTable(data);
        buildActionButtons(data);
    });
  }

  function renderTable(data) {
    var gridData = data['GridData'];
    
    for (var i = 0; i < gridData.length; i++) {
      renderRow(gridData[i]);
    }
  }

  function buildActionButtons(data) {
    var actionButtons = data['ActionButtons'];
    var imageNames = [];
    var actionButtonsMap;

    for (var i = 0; i < actionButtons.length; i++) {
      imageNames.push(actionButtons[i].ImageName);
    }

    actionButtonsMap = imageNames.map(function(actionButton, index) {
      var obj = {};
      var key = actionButton;
      
      switch(key) {
        case 'upload':
          obj[key] = 'glyphicon-upload';
          break;
        case 'create_folder':
          obj[key] = 'glyphicon-folder-open'
          break;
        case 'share':
          obj[key] = 'glyphicon-share'
          break;
        case 'cut':
          obj[key] = 'glyphicon-remove'
          break;
        case 'tag':
          obj[key] = 'glyphicon-cloud'
          break;
        case 'copy':
          obj[key] = 'glyphicon-copy'
          break;
        case 'paste':
          obj[key] = 'glyphicon-paste'
          break;
        case 'search':
          obj[key] = 'glyphicon-search'
          break;
        case 'folder_settings':
          obj[key] = 'glyphicon-option-vertical'
          break;
        case 'send_email':
          obj[key] = 'glyphicon-send'
          break;
      }

      return obj;
    });

    // render the buttons to interface
    renderActionButtons(actionButtonsMap);
  }

  function renderRow(row) {
    var tr = $("<tr />");
    $("#interface tbody").append(tr);
    tr.append($("<td>" + row.Name + "</td>"));
    tr.append($("<td>" + row.Description + "</td>"));
    tr.append($("<td>" + row.ModifiedDate + "</td>"));
  }

  function renderActionButtons(actionButtonsMap,index) {
    var count = 1;
    for (var i = 0; i < actionButtonsMap.length; i++) {
      // append each button into <div class="action-buttons" id="colX"></div>
      // allow 4 buttons per col
      var button;
      for (var key in actionButtonsMap[i]) {
        button = $("<button type='button' class='btn btn-default'></button>");
        span = $("<span class='glyphicon'></span>");
        span.addClass(actionButtonsMap[i][key]);
        button.append(span);
      }
      
      // append to appropriate col
      if ( count <= 4 ) {
        $("#col1").append(button);
        count++;
      }
      else if ( count > 4 && count <= 8) {
        $("#col2").append(button);
        count++;
      }
      else {
        $("#col3").append(button);
        count++;
      }
    }
    // var tr = $("<tr />");
    // $("#action-buttons tbody").append(tr);
    // tr.append($("<td>" + row.Name + "</td>"));
  }

  $('.toggle-actions').click(function() {
    if ($('.toggle-actions').hasClass('collapse-button')) {
      $('.toggle-actions').removeClass('collapse-button');
      $('.toggle-actions').addClass('expand');
      $('.toggle-actions').children().removeClass('glyphicon-arrow-left');
      $('.toggle-actions').children().addClass('glyphicon-arrow-right');
      $('#interface-wrapper').width(855);
      $('#col2').hide();
      $('#col3').hide();
    }
    else if ($('.toggle-actions').hasClass('expand')) {
      $('.toggle-actions').removeClass('expand');
      $('.toggle-actions').addClass('collapse-button');
      $('.toggle-actions').children().removeClass('glyphicon-arrow-right');
      $('.toggle-actions').children().addClass('glyphicon-arrow-left');
      $('#interface-wrapper').width(968);
      $('#col2').show();
      $('#col3').show();
    }
  });
});